package emma_wanmei;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for ShiftedTalker
 */

public class ShiftedTalkerTest {

      /**
     * Throw error if the length of message is less than the shifted index 
     */
    @Test
    public void test1()
    {
        try
        {
            ShiftedTalker st = new ShiftedTalker(6);
            st.talk("hello");
            fail("The word can not be shifted because the length of message is less than the shifted index");
        }
        catch(IllegalArgumentException e)
        {
            System.out.print("Error successfully thrown");
        }
        
    }

    /**
     * If shifted index is 0,the message should not change
     */
    @Test
    public void test2()
    {
        ShiftedTalker st = new ShiftedTalker(0);
        String result = st.talk("HelloWorld");// not sure how to deal with talk method
        String message = "HelloWorld";
        assertEquals(message, result); // expect no change in the message
    }

    /**
     * Check if the shifted order correct 
     */
    @Test
    public void test3()
    {
        ShiftedTalker st = new ShiftedTalker(3);
        String result = st.talk("abcdef"); // not sure how to deal with talk method
        String expectedMessage = "defabc";
        assertEquals(expectedMessage, result);// expected the same result
    }

}



