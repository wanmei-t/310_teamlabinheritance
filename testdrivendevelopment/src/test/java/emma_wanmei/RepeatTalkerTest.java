package emma_wanmei;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for RepeatTalker
 */
public class RepeatTalkerTest {
    /**
     * Can't create a RepeatTalker with input of less than 0
     */
    @Test
    public void test1()
    {
        RepeatTalker repeatTalker = new RepeatTalker(-1);
        fail("A repeatTalker shoudln't be successfully created with an input of less than 0");
    }

    /**
     * Can create a RepeatTalker with 0 or more
     */
    @Test
    public void test2()
    {
        RepeatTalker repeatTalker1 = new RepeatTalker(0);
        RepeatTalker repeatTalker2 = new RepeatTalker(2);
    }

    /**
     * talk() should return empty string
     */
    @Test
    public void test3()
    {
        RepeatTalker repeatTalker = new RepeatTalker(0);
        assertEquals(repeatTalker.talk("hello"), "");
    }

    /**
     * talk() should return "hellohellohello"
     */
    @Test
    public void test4()
    {
        RepeatTalker repeatTalker = new RepeatTalker(3);
        assertEquals(repeatTalker.talk("hello"), "hellohellohello");
    }
}
