package emma_wanmei;

public class ShiftedTalker extends Talker{
    private int shiftedIndex;
    
    public ShiftedTalker(int shiftedIndex)
    {
        this.shiftedIndex = shiftedIndex;
    }

    public String talk(String message)
    {
        if (message.length()-1<shiftedIndex)
        {
            throw new IllegalArgumentException("The message is too small");
        }
        String result = "";
        for (int i = this.shiftedIndex; i < message.length(); i++)
        {
            result += message.charAt(i);
        }
        for (int i = 0; i < this.shiftedIndex; i++)
        {
            result += message.charAt(i);
        }
        return result;
    }
}
