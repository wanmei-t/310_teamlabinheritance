package emma_wanmei;

public class RepeatTalker extends Talker
{
    private int n;
    
    public RepeatTalker(int n)
    {
        this.n = n;
    }
  
    public String talk(String repeatText)
    {
        if (n < 0) 
        {
            throw new IllegalArgumentException("The value of n can not be negative.");
        }
        String result = "";
        for (int i = 0; i < n; i++) 
        {
            result += repeatText;
        }    
        return result;
    }
}